package abb.excercise19.controller;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(path="/stude")
public class StudentController {

    @GetMapping
    public String getStudent(){
        return "Student";
    }
}
