package abb.excercise19;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Excercise19Application {

    public static void main(String[] args) {
        SpringApplication.run(Excercise19Application.class, args);
    }

}
