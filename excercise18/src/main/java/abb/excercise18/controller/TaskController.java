package abb.excercise18.controller;

import abb.excercise18.dto.StudentRequest;
import abb.excercise18.dto.StudentResponse;
import abb.excercise18.dto.TaskRequest;
import abb.excercise18.dto.TaskResponse;
import abb.excercise18.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/task")
@RequiredArgsConstructor
public class TaskController {

    private final TaskService taskService;

    @PostMapping(path = "/save")
    public TaskResponse createTask(@RequestBody TaskRequest taskRequest){
        return taskService.createTask(taskRequest);
    }

    @GetMapping(path ="/all")
    public Iterable<TaskResponse> getAllTasks(){
        return taskService.getAllTasks();
    }

    @DeleteMapping(path ="/delete/{id}")
    public void removeTask(@PathVariable Integer id){
        taskService.removeTask(id);
    }

    @GetMapping(path ="/task/{id}")
    public List<TaskResponse> getTaskById(@PathVariable List<Integer> id){
        return taskService.findTaskById(id);
    }

}
