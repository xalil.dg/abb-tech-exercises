package abb.excercise18.controller;

import abb.excercise18.entity.Student;
import abb.excercise18.service.StudentService;
import abb.excercise18.dto.StudentRequest;
import abb.excercise18.dto.StudentResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/student")
@RequiredArgsConstructor
public class StudentController {

    private final StudentService studentService;

    @PostMapping(path = "/save")
    public StudentResponse createStudent(@RequestBody StudentRequest student){
        return studentService.createStudent(student);
    }

    @GetMapping("/all")
    public List<StudentResponse> getStudentList(){
        return studentService.getAllStudents();
    }


    @DeleteMapping("/delete/{id}")
    public void removeStudent(@PathVariable Integer id){
        studentService.removeStudent(id);
    }

    @GetMapping("/{id}")
    public List<StudentResponse> getStudentById(@PathVariable List<Integer> id){
        return studentService.findStudentById(id);
    }

    @PutMapping("/update/{id}")
    public StudentResponse updateStudent(@PathVariable Integer id, @RequestBody StudentRequest student){
        return studentService.updateStudent(id, student);
    }

}
