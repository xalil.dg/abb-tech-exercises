package abb.excercise18.service;

import abb.excercise18.dto.TaskRequest;
import abb.excercise18.dto.TaskResponse;

import java.util.List;

public interface TaskService {

    List<TaskResponse> getAllTasks();
    TaskResponse createTask(TaskRequest taskRequest);

    void removeTask(Integer id);

    List<TaskResponse> findTaskById(List<Integer> id);
}
