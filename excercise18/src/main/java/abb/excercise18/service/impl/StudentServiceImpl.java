package abb.excercise18.service.impl;

import abb.excercise18.dto.StudentRequest;
import abb.excercise18.dto.StudentResponse;
import abb.excercise18.entity.Student;
import abb.excercise18.exceptions.UserNotFoundException;
import abb.excercise18.repository.StudentRepository;
import abb.excercise18.service.StudentService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class StudentServiceImpl implements StudentService {

    private final StudentRepository studentRepository;

    @Override
    public List<StudentResponse> getAllStudents() {
        List<StudentResponse> studentResponses = new ArrayList<>();
        List<Student> students = studentRepository.getAllStudents();
        for (Student student : students) {
            StudentResponse response = new StudentResponse();
            response.setName(student.getName());
            response.setEmail(student.getEmail());
            response.setTaskId(student.getTaskId());
            studentResponses.add(response);
        }
        return studentResponses;
    }

    @Override
    public StudentResponse createStudent(StudentRequest student) {
        return mapToStudentResponse(studentRepository.createStudent(mapToStudent(student)));
    }

    @Override
    public void removeStudent(Integer id) {
        studentRepository.removeStudent(id);
    }

    @Override
    public List<StudentResponse> findStudentById(List<Integer> ids) {
        List<Student> students = studentRepository.findStudentById(ids);
        if (!students.isEmpty()) {
            return students.stream().map(this::mapToStudentResponse).toList();
        }
        throw new UserNotFoundException("User not found");
    }

    @Override
    public StudentResponse updateStudent(Integer id, StudentRequest studentRequest) {
        Student student = mapToStudent(studentRequest);
        Student updating = studentRepository.updateStudent(id, student);
        return mapToStudentResponse(updating);
    }


    private Student mapToStudent(StudentRequest studentRequest) {
        Student student = new Student();
        student.setName(studentRequest.getName());
        student.setEmail(studentRequest.getEmail());
        student.setTaskId(studentRequest.getTaskId());
        return student;
    }

    private StudentResponse mapToStudentResponse(Student student) {
        StudentResponse response = new StudentResponse();
        response.setName(student.getName());
        response.setEmail(student.getEmail());
        response.setTaskId(student.getTaskId());
        return response;
    }
}
