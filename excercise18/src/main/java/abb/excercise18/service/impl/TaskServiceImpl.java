package abb.excercise18.service.impl;

import abb.excercise18.dto.TaskRequest;
import abb.excercise18.dto.TaskResponse;
import abb.excercise18.entity.Task;
import abb.excercise18.repository.TaskRepository;
import abb.excercise18.service.TaskService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

@Service
@RequiredArgsConstructor
public class TaskServiceImpl implements TaskService {

    private final TaskRepository taskRepository;
    @Override
    public List<TaskResponse> getAllTasks() {
        List<TaskResponse> taskResponses = new ArrayList<>();
        List<Task> tasks = taskRepository.getAllTasks();
        for (Task task : tasks) {
            TaskResponse response = new TaskResponse();
            response.setTitle(task.getTitle());
            response.setDescription(task.getDescription());
            response.setStudentId(task.getStudentId());
            taskResponses.add(response);
        }
        return taskResponses;
    }


    @Override
    public TaskResponse createTask(TaskRequest taskRequest) {
        return mapToTaskResponse(taskRepository.createTask(mapToTask(taskRequest)));
    }

    @Override
    public void removeTask(Integer id) {
        taskRepository.removeTask(id);
    }

    @Override
    public List<TaskResponse> findTaskById(List<Integer> ids) {
        List<Task> task = taskRepository.findTaskById(ids);
        if (!task.isEmpty()) {
            return task.stream().map(this::mapToTaskResponse).toList();
        }
        return Collections.emptyList();
    }


    private Task mapToTask(TaskRequest taskRequest) {
        Task task=new Task();
        task.setTitle(taskRequest.title());
        task.setDescription(taskRequest.description());
        task.setStudentId(taskRequest.studentId());
        return task;
    }

    private TaskResponse mapToTaskResponse(Task task) {
        TaskResponse response=new TaskResponse();
        response.setTitle(task.getTitle());
        response.setDescription(task.getDescription());
        response.setStudentId(task.getStudentId());
        return response;
    }
}
