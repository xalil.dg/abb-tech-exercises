package abb.excercise18.service;

import abb.excercise18.entity.Student;
import abb.excercise18.dto.StudentRequest;
import abb.excercise18.dto.StudentResponse;

import java.util.List;

public interface StudentService {

    List<StudentResponse> getAllStudents();
    StudentResponse createStudent(StudentRequest student);

    void removeStudent(Integer id);

    List<StudentResponse> findStudentById(List<Integer> id);

    StudentResponse updateStudent(Integer id,StudentRequest student);
}
