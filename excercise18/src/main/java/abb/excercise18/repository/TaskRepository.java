package abb.excercise18.repository;

import abb.excercise18.entity.Student;
import abb.excercise18.entity.Task;

import java.util.List;

public interface TaskRepository {

    List<Task> getAllTasks();

    Task createTask(Task task);

    void removeTask(Integer id);

    List<Task> findTaskById(List<Integer> id);
}
