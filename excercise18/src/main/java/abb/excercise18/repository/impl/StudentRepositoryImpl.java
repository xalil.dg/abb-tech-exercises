package abb.excercise18.repository.impl;

import abb.excercise18.entity.Student;
import abb.excercise18.repository.StudentRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
@RequiredArgsConstructor
@Slf4j
public class StudentRepositoryImpl implements StudentRepository {

    private final JdbcTemplate jdbcTemplate;

    public List<Student> getAllStudents() {
        return jdbcTemplate.query("select * from student", (resultSet, i) -> {
            Student student = new Student();
            student.setName(resultSet.getString("name"));
            student.setEmail(resultSet.getString("email"));
            student.setTaskId(resultSet.getInt("task_id"));
            return student;
        });
    }

    public Student createStudent(Student student) {
        jdbcTemplate.update("""
                insert into student (name, email, task_id)
                values (?,?,?)
                """, student.getName(), student.getEmail(), student.getTaskId());
        return student;
    }


    @Override
    public void removeStudent(Integer id) {
        jdbcTemplate.update("delete from student where id =?", id);
    }

    @Override
    public List<Student> findStudentById(List<Integer> ids) {
        String inSql = String.join(",", Collections.nCopies(ids.size(), "?"));
        List<Student> students = jdbcTemplate.query(
                String.format("select * from student where id in (%s)", inSql),
                ids.toArray(),
                (rs, rowNum) -> new Student(rs.getInt("id"), rs.getString("name"),
                        rs.getString("email"), rs.getInt("task_id")));
        return students;
    }

    @Override
    public Student updateStudent(Integer id, Student student) {
        Student findStudent = findById(id);
        jdbcTemplate.update("update student set name=?, email=?, task_id=? where id=?",
                student.getName(), student.getEmail(), student.getTaskId(), id);
        return student;
    }

    public Student findById(Integer id) {
        String sql = "select * from student where id=?";
        Student student = jdbcTemplate.queryForObject(sql,
                (rs, num) -> new Student(rs.getInt("id"), rs.getString("name"),
                        rs.getString("email"), rs.getInt("task_id")), id);
        log.info("Studentttttt {} ", student.getName());
        return student;
    }

}
