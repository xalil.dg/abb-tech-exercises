package abb.excercise18.repository.impl;

import abb.excercise18.entity.Task;
import abb.excercise18.repository.TaskRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Repository;

import java.util.Collections;
import java.util.List;

@Repository
@RequiredArgsConstructor
@Slf4j
public class TaskRepositoryImpl implements TaskRepository {

    private final JdbcTemplate jdbcTemplate;

    @Override
    public List<Task> getAllTasks() {
        return jdbcTemplate.query("select * from task", (resultSet, i) -> {
            Task task = new Task();
//            task.setId(resultSet.getInt("id"));
            task.setTitle(resultSet.getString("description"));
            task.setDescription(resultSet.getString("title"));
            task.setStudentId(resultSet.getInt("student_id"));
            log.info("Taskkk {} ",task.getDescription());
            return task;
        });
    }

    @Override
    public Task createTask(Task task) {
        jdbcTemplate.update("""
                insert into task (description, student_id, title)
                values (?,?,?)
                """, task.getDescription(), task.getStudentId(), task.getTitle());
        return task;
    }

    @Override
    public void removeTask(Integer id) {
        jdbcTemplate.update("delete from task where id =?", id);
    }

    @Override
    public List<Task> findTaskById(List<Integer> id) {
        String sql = String.join(",", Collections.nCopies(id.size(), "?"));
        List<Task> tasks = jdbcTemplate.query(
                String.format("select * from task where id in(%s)", sql),
                id.toArray(),
                (rs, rowNum) -> new Task(rs.getInt("id"), rs.getString("title"),
                        rs.getString("description"), rs.getInt("student_id")));
        return tasks;
    }

}
