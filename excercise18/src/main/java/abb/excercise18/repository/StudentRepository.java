package abb.excercise18.repository;

import abb.excercise18.entity.Student;

import java.util.List;

public interface StudentRepository {

    List<Student> getAllStudents();

    Student createStudent(Student student);

    void removeStudent(Integer id);

    List<Student> findStudentById(List<Integer> id);

    Student updateStudent(Integer id, Student student);
}
