package abb.excercise18.exceptions;

import abb.excercise18.dto.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;

public class ApiExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(UserNotFoundException.class)
    public final ResponseEntity<ErrorResponse> userNotFound(Exception ex){
        ErrorResponse response=new ErrorResponse(LocalDateTime.now(), HttpStatus.NOT_FOUND,ex.getMessage());
        return new ResponseEntity<>(response,HttpStatus.EXPECTATION_FAILED);
    }

    @ExceptionHandler(Exception.class)
    public final ResponseEntity<ErrorResponse> exception(Exception ex){
        ErrorResponse response=new ErrorResponse(LocalDateTime.now(), HttpStatus.NOT_FOUND,ex.getMessage());
        return new ResponseEntity<>(response,HttpStatus.EXPECTATION_FAILED);
    }
}
