package abb.excercise18.dto;

public record TaskRequest(

        String title,
        String description,
        Integer studentId
) {
}
