package abb.excercise18;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Excercise18Application {

    public static void main(String[] args) {
        SpringApplication.run(Excercise18Application.class, args);
    }

}
